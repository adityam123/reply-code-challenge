export const CONSTANTS = {
	INPUT_FILE_BASE: 'input/',
	OUTPUT_FILE_BASE: 'output/',
};

export const POINTS: { [key: string]: number | null } = {
	T: 50,
	H: 70,
	_: 100,
	X: 120,
	'+': 150,
	'*': 200,
	'~': 800,
	'#': null,
};
