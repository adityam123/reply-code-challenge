"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fileHandler = __importStar(require("./helpers/filehandler"));
var resource_processing_1 = require("./helpers/resource-processing");
var page_data_class_1 = require("./model/page-data.class");
console.log = function () { };
var processFile = function (fileName) {
    var fileContents = fileHandler
        .readFile(fileName)
        .split('\n')
        .map(function (line) { return line.replace('\r', ''); });
    var _a = fileContents[0].split(' ').map(function (value) { return parseInt(value, 10); }), W = _a[0], H = _a[1];
    var companyMap = fileContents.slice(1, H + 1).map(function (line) { return line.split(''); });
    var developerCount = parseInt(fileContents[H + 1], 10);
    var developers = [];
    for (var i = 0; i < developerCount; i++) {
        var _b = fileContents[i + H + 2].split(' ').map(function (value, index) {
            return [1, 2].includes(index) ? parseInt(value, 10) : value;
        }), company = _b[0], bonusPoints = _b[1], skillCount = _b[2], skills = _b[3];
        developers.push({ company: company, bonusPoints: bonusPoints, skillCount: skillCount, skills: skills, placed: false });
    }
    var projectManagerCount = parseInt(fileContents[H + developerCount + 2], 10);
    var projectManagers = [];
    for (var i = 0; i < projectManagerCount; i++) {
        var _c = fileContents[i + H + 3 + developerCount].split(' ').map(function (value, index) {
            return index === 1 ? parseInt(value, 10) : value;
        }), company = _c[0], bonusPoints = _c[1];
        projectManagers.push({ company: company, bonusPoints: bonusPoints, placed: false });
    }
    return new page_data_class_1.PageData({ W: W, H: H }, developers, projectManagers, companyMap);
};
var fileName = 'a_solar.txt';
var data = processFile(fileName);
var finalData = JSON.stringify(resource_processing_1.getIndices(data))
    .replace('[', '')
    .replace(']', '')
    .replace(/","/g, '\n')
    .replace(/"/g, '')
    .replace(/,/g, ' ');
console.log(finalData);
fileHandler.writeFile(fileName, finalData);
// console.log(JSON.stringify(data.companyMap));
