"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var constants_1 = require("./constants");
function readFile(fileName) {
    return fs_1.default.readFileSync(constants_1.CONSTANTS.INPUT_FILE_BASE + fileName, 'utf8');
}
exports.readFile = readFile;
function writeFile(fileName, data) {
    return fs_1.default.writeFileSync(constants_1.CONSTANTS.OUTPUT_FILE_BASE + fileName, data);
}
exports.writeFile = writeFile;
