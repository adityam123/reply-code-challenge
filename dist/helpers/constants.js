"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CONSTANTS = {
    INPUT_FILE_BASE: 'input/',
    OUTPUT_FILE_BASE: 'output/',
};
exports.POINTS = {
    T: 50,
    H: 70,
    _: 100,
    X: 120,
    '+': 150,
    '*': 200,
    '~': 800,
    '#': null,
};
