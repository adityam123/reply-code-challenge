"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PageData = /** @class */ (function () {
    function PageData(metaData, developers, projectManagers, companyMap) {
        this.metaData = { W: 0, H: 0 };
        this.developers = [];
        this.projectManagers = [];
        this.companyMap = [];
        this.metaData = metaData;
        this.developers = developers;
        this.projectManagers = projectManagers;
        this.companyMap = companyMap;
    }
    return PageData;
}());
exports.PageData = PageData;
